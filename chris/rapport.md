## Rapport de mise en situation professionnelle 



# "Mettre en œuvre des méthodes/outils permettant d’aider le responsable des équipes de développeurs à mieux gérer les développements"





<img src="./img/devops3.png" alt="sigle DevOps" style="width:400px" />








## Christophe Paris
### Formation DevOps - Estia 2020


<div style="page-break-after: always;"></div>

## Sommaire





[Introduction](#introduction)



1.[Contexte](#contexte)


2.[Mise en place d'un environnement technique adapté](#mep_tech)

   a. [Méthodologie Agile](#agile)

   b. [Choix d'un IDE](#ide)

   c. [Utilisation d'un outil de gestion de version de code source](#git)

   d. [Résolution et anticipation des bugs]("tdd")

   e. [Utilisation de containers](#docker)

   f. [Utilisation d'un hébergeur de code source et serveur d'intégration continue](#gitlab)

   g. [Elaboration de la documentation](#doc)

3.[Coordination du projet et pistes d'amélioration](#pistes)

​	a. [Assistance utilisateur](#helpdesk)

​	b. [Aspects juridiques](#juridique)



[Conclusion](#conclusion)



<div style="page-break-after: always;"></div>

## Introduction <a id="introduction"></a>



Dans le cadre de la formation DevOps 2020 dispensée à l'Estia ( Bidart, 64) Il nous a été proposé une mise en situation professionnelle. Cette mise en situation a pour but de valider nos compétences acquises lors de la formation sur la mise en oeuvre de l'intégration continue.

Lors de la réalisation technique, nous avons travaillé en équipe ce qui nous a permis d'apréhender et d'utiliser certaines pratiques de la méthodologie Agile, toutefois, le rapport et la présentation de notre travail se fera de manière individuelle.

Le groupe de travail était constitué de Geoffrey Limantour, Terrence Biernaczyk et moi même.

Ayant eu la possibilité de choisir une base de travail différente que celle proposée ([Data Retriever](https://github.com/hupi-analytics/data-retriever)), et dans l'optique de travailler sur le cycle de vie d'une application dans son ensemble, nous avons fait le choix de nous appuyer sur une API en javascript (Node JS) créée par nos soins: [https://gitlab.com/tgc1/api](https://gitlab.com/tgc1/api). Cette API s'appuie sur une base de donnée MongoDB de type NoSQL.

Tout comme dans le scénario initial, cette API pourrait être appelée par une ou plusieurs apllications web. 

Pour l'exemple nous avons créé une application web interrogeant notre plateforme en utilisant React JS: [https://gitlab.com/tgc1/app](https://gitlab.com/tgc1/app). Il s'agit d'une simple Todo App qui dans son état actuel permet d'ajouter des tâches à une liste.





## 1. Contexte <a id="contexte"></a>



La société Massive Data requiert notre expertise pour automatiser le processus de livraison de ses différentes applications.

Lors de l'analyse du cahier des charges, il est ressorti plusieurs problématiques auxquelles nous allons tenter d'apporter une solution:

* Gestion et compatibilité des versions
* Découverte par le client de bugs non identifiés
* Suivi de livraisons et communication des mises à jour aux clients
* Processus de déployement trop long
* Documentations obsolètes
* Plannification et répartition des demandes clients 

Compte tenu de ces problématiques, Le directeur technique a du mal à tenir informé sa direction des livraisons et de leurs statuts.





## 2. Mise en place de l'environnement technique adapté <a id="mep_tech"></a>

### a. Méthodologie Agile <a id="agile"></a>

Tout d'abord afin d'améliorer la communication dans l'équipe et dans l'objectif d'effectuer un pilotage des projets en cours de développement plus sécurisé, il nous semble important de mettre en place des méthodologies de travail collaborative issues de la **méthode Agile**.

**Scrum** _(mêlée en anglais)_ est la méthodologie agile la plus répandue pour la gestion de projet et a pour but d'améliorer la productivité des équipes. 

Le cycle de vie Scrum est rythmé par des **sprints**  qui sont des itérations de 2 à 4 semaines. Avant chaque sprint, on organise un sprint planning meeting ou le Product Owner priorise les fonctionnalités du backlog à forte valeur ajoutée pour le client qui seront à développer au cours du sprint.





<img src="./img/scrum.png" alt="scrum" style="width:800px" />





Tous les jours est organisé un **daily meeting**, ou mêlée. Il s'agit d'une courte réunion ou chaque membre de l'équipe fait part de l'avancement des tâches qu'il a effectuées, les difficultées rencontrées, et les tâches qu'il prévoit d'éffectuer dans la journée.

A la fin d’un sprint, on fait une démonstration au client des derniers développements, le **Sprint Review Meeting**. C’est aussi l’occasion de faire un bilan sur le fonctionnement de l’équipe et de **trouver des points d’amélioration**.

A partir du cahier des charges initial, on réalise un **product backlog** qui contient les fonctionnalités attendues sur le projet. Il n'est pas figé et est destiné à évoluer dans le temps en fonction des besoins du client.

<div style="page-break-after: always;"></div>

Il y a trois rôles clés dans une équipe Scrum:

Le **Scrum Master** qui est le garant des principes Scrum. Son rôle est de faciliter la communication au sein des équipes et d'améliorer ainsi la productivité. C'est le Scrum Master qui dirige les différentes réunions.

Le **Product Owner** qui a la vision globale du projet. Son rôle est d'établir les priorités des fonctionnalités à développer en tenant compte de la valeur ajoutée pour le client mais aussi de la capacité de l'équipe à les réaliser dans le temps imparti (sprint)

**L'équipe** qui participe au projet.

### b. Choix d'un IDE:  <a id="ide"></a>



Un IDE _(Integrated Development Environment ou Environnement de développement)_ regroupe un ensemble d’outils spécifiques dédiés aux programmeurs afin qu’ils puissent **optimiser leur temps de travail** et **améliorer leur productivité**.

Il est l'outil principal du développeur, c'est pourquoi il est primordial de bien le connaître et de se l'approprier pour une utilisation efficace.

Nous avons choisi d'utiliser **Visual Studio Code** car il est crossplatform, gratuit et hautement personnalisable grâce à ses nombreux plugins. Il supporte plus de 30 langages de programmation et est mis à jour tous les mois grâce à une communauté nombreuse et dynamique.



  <img src="./img/vsc_logo.png" alt="vs code logo" style="width:400px; margin-left:20%"  />



L'utilisation de certaines extensions facilite le travail des développeurs. Ils peuvent ainsi rester focaliser sur leur IDE sans avoir à faire des va-et-viens incessants entre d'autres outils.

_L'utilisation du même IDE pour tous les développeurs n'est pas une obligation, mais dans un soucis de maintenance, support et homogénéité des outils de développement, c'est conseillé._

> Autres IDE: Vim, Atom, Sublime Text, Eclipse, Visual Studio, ...



<div style="page-break-after: always;"></div>

**Processus d'installation et de configuration**

1. Téléchargez le packet d'installation correspondant à votre OS sur [la page de téléchargement de Visual Studio Code](https://code.visualstudio.com/download) puis lancerzl'installation.

2. Lancez VsCode dans un répertoire à partir du terminal

```sh
code .    # ouvre le répertoire courant dans vscode

code app  # ouvre le répertoire app dans vscode
```

3. VsCode intègre également un terminal que l'on peut ouvrir en cliquant sur l'onglet `terminal` ou en faisant glisser la souris vers le haut de la barre des tâches.

4. Les fichiers de configuration, raccourcis claviers, snippets se trouvent dans `File > Preferences`

5. Les Extensions peuvent se télécharger sur le store directement à partir de l'application en cliquant sur l'onglet `Extensions` dans la barre latérale. 

Pour notre projet, majoritairement développé en javascript, nous conseillons d'utiliser les extensions suivantes:

**ESLint** qui est un outil d'analyse de syntaxe du code. Les règles sont personnalisables ce qui permet l'uniformisation du code pour tous les développeurs.

**npm** qui valide les dépendances du projet décrites dans le fichier de configuration `package.json`

**Auto Close Tag** qui fait gagner du temps et évite des erreurs en ajoutant automatiquement les balises fermantes

**Docker** qui permet de voir les images présentes et les containers en cours d'éxecution sur sa machine.

**GitLab Workflow** qui permet de voir en direct les différentes issues, merge request ou pipelines relatives au projet.

**GitLens** qui surcharge le client **Git** intégré dans vscode en lui apportant plus de fonctionnalités.

**Markdownlint** qui contient de nombreux snippets et intègre un linter  pour les fichiers MarkDown très utiles pour rédiger la documentation.



### c. Utilisation d'un outil de gestion de version de code source  <a id="git"></a>



Pour palier à la difficultée de gérer les versions de nos différentes applications, nous préconisons l'utilisation de **Git**. 

Git est un gestionnaire décentralisé de version de code. Cela signifie que chaque développeur a toutes les version de code du projet en local sur son poste de travail. 

Il est un outil idéal pour le travail collaboratif car il permet, grâce à des "branches", de développer des fonctionnalités en paralèlle sans risquer d'écraser le travail d'un autre.

Une autre de ses caractéristiques est de pouvoir revenir en arrière sur un commit précédent _(modifications sauvegardées)_ afin d'annuler des erreurs.

<div style="page-break-after: always;"></div>

**Installation _sur environnement Linux_**

```sh
sudo apt-get update

sudo apt-get install git-all

git --version     # Doit afficher la version installée
```

La première chose à faire après l’installation de Git est de renseigner votre nom et votre adresse  email. C’est une information importante car toutes les validations dans Git utilisent cette information et elle est indélébile dans toutes les validations que vous pourrez réaliser

```sh
git config --global user.name "John Doe"

git config --global user.email johndoe@example.com
```


**Utilisation des commandes basiques de Git**

Initialiser un nouveau dépôt Git:

```sh
  git init
```

Checker l'état des fichiers du dépôt:

```sh
  git status
```

Ajouter des fichiers non suivis:

```sh
  git add
```

"Commiter" ses modifications:

```sh
  git commit -m "Message d'identification du commit"
```

Cloner un dépôt distant:

```sh
  git clone <CHEMIN_DISTANT_DU_DEPOT>
```

Créer une nouvelle branche et basculer dessus:

```sh
  git checkout -b <NOM_NOUVELLE_BRANCHE>
```

Récupérer les dernières modifications du dépôt distant

```sh
git pull
```

Pousser ses commits sur un dépôt distant

```sh
git push
```
<div style="page-break-after: always;"></div>

### d. Détection, anticipation et résolution des bugs <a id="tdd"></a>


Dans l'optique de limiter l'apparition de bugs en production, nous préconisons à l'équipe de développement d'appliquer la méthodologie **TDD** _(Test Driven Developpment)_. Cette méthode consiste à développer les **tests unitaires** avant de coder une fonctionnalité.

Les étapes du cycle de développement préconisé par TDD :

- Ecriture d'un premier test découlant d'une user story
- Exécuter le test et vérifier qu'il échoue (car le code n'a pas encore été implémenté)
- Implémentation de la fonctionnalité pour faire passer le test
- Exécution des tests afin de contrôler que les tests passent
- Remaniement (Refractor) du code afin d'en améliorer la qualité mais en conservant les mêmes fonctionnalités

	<img src="./img/tdd.png" alt="cycle TDD" style="width:50%" />

Grâce aux tests unitaires, on évite les problèmes de régression. Lorsqu'un développeur modifie une fonctionnalité existante, ou en code une nouvelle, il peut relancer les tests unitaires afin de s'assurer que sa modification n'a pas impacté l'existant. Dans le cadre de l'intégration continue, ces tests pourront être automatisés à chaque nouveau commit poussé sur le dépôt distant.

Pour notre API, nous avons utilisé le framework **UnitJs** et **Mocha** pour réaliser et lancer les tests unitaires. Pour lancer les tests en local, suivez la procédure de lancement de l'api puis ouvrez un autre terminal, entrez dans le container de l'api puis lancez la commande `yarn test`.



<img src="./img/test.png" alt="test" style="width:600px" />

<div style="page-break-after: always;"></div>

### e. Utilisation des containers  <a id="docker"></a>



Afin d'assurer la compatibilité des versions de nos applications et leurs dépendances, nous préconisons l'utilisation de **Docker**.

Docker est un outil qui peut empaqueter une application et ses dépendances dans un conteneur isolé, qui pourra être exécuté sur n'importe quel serveur et n'importe quel environnement.

Chaque image construite a son propre environnement, sa version, ses dépendances avec des versions figées, ce qui limite fortement les risques d'incompatibilité entre les applications.

**Installation**

[Documentation officielle](https://docs.docker.com/engine/install/ubuntu/)

Mise à jour du package manager `apt`, installation des dépendances requises, ajout du repository, et installation de Docker:

```sh
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

```sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) \
    stable"
```

```sh
sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Ajouter Docker à `usermod` afin de pouvoir l'utiliser comme simple utilisateur (sans `sudo`)

```sh
sudo usermod -aG docker your-user
```

Vérifier l'installation en lançant le container `hello-world`

```sh
docker run hello-world
```
<div style="page-break-after: always;"></div>

**Commandes principales**

Construire une image à l’aide d’un Dockerfile

```sh
  docker build -t <NAME>:<TAG> .
```

> Le tag est optionnel, par défaut le tag est « latest ».
> Ne pas oublier le point pour le répertoire courant.


Démarrer un container à partir d’une image

```sh
docker run <Name>:<Tag>
```

> Pour ouvrir un bash dans le container, passez le paramètre `-it` avant le nom de l’image et `bash` après.
> Pour lancer le container en tâche de fond, passez le paramètre `-d` avant le nom de l’image.


Vérifier les images présentes sur la machine

```sh
docker image ls
```

Vérifier les containers qui tournent sur la machine 

```sh
docker ps
```


Dans notre contexte, le code de notre plateforme API contient un **Dockerfile** qui nous permet de construire une image Docker.

Docker-compose nous permet de lier, au sein d'un même réseau (**network**), l'image de notre API lancée dans un container, à une image MongoDB lancée dans un autre container, le tout dans un environnement stable et sécurisé.


**Le processus de lancement de notre API est le suivant :**

A la racine de l'API 

```sh
docker-compose up
```

Cette action exécute le processus listé dans le fichier `docker-compose.yml` et lance donc nos 2 containers API et Mongo. 

Laisser tourner le processus MongoDB et ouvrir un aure terminal dans lequel nous allons entrer dans le container de l'API afin d'installer les dépendances et de lancer le serveur.

```sh
docker-compose exec api bash
```

Une fois dans le container:

```sh
yarn
yarn dev
```
Notre API est lancée et nous pouvons commencer à lui envoyer des requêtes sur l'URL `localhost:8080`

<div style="page-break-after: always;"></div>

### f. Utilisation d'un hébergeur de code source et serveur d'intégration continue  <a id="gitlab"></a>



Enfin, nous devons utiliser un service d'hébergement et de gestion de développement logiciel utilisant Git. Les trois principaux sont Github, Gitlab et Bitbucket.

Notre choix s'est porté sur **Gitlab** car il est open source, gratuit, et intègre de nombreux services pour le développement logiciel dont un serveur d'intégration continue et un service de gestion des tickets. Nous pouvons également héberger nos images Docker directement sur Gitlab car il intègre un registry. 

Grâce aux webhooks, on reste toujours informé des évolutions du projet car on peut lier des événements Gitlab à des outils de gestion de projet tels que Trello Jira mais aussi à des plateformes de communication collaborative utilisée dans les entreprises tels que Slack, Discord, Rocket Chat, ...



<img src="./img/discord.png" alt="discord" style="width:800px" />



A travers cet outil, le directeur technique pourra observer très rapidement quelles sont les versions d'applications en production, suivre la progression de ses équipes pour le sprint en cours, attribuer certains tickets à des développeurs.



<img src="./img/boards.png" alt="boards" style="width:800px" />



Il peut contrôler les statuts des déploiements grâce aux pipelines d'intégration continue.



<img src="./img/pipeline.png" alt="pipeline" style="width:800px" />





Afin d'automatiser toutes les tâches d'intégration et de déploiement continue nous avons besoin d'installer **Gitlab runner** qui permettera à Gitlab de monter l'application dans un container afin de la tester et d'effectuer toutes les étapes décrites dans un fichier de configuration `.gitlab-ci.yml` qui doit être placé à la racine de notre projet.

Il y a plusieurs procédures d'installation détaillées sur la [documentation officielle](https://docs.gitlab.com/runner/install/)

<div style="page-break-after: always;"></div>

**Configuration de la pipeline**

Dans notre cas notre pipeline définie dans le fichier de configuration [.gitlab-ci.yml](https://gitlab.com/tgc1/api/-/blob/master/.gitlab-ci.yml) comporte quatre **"jobs"** plus une étape de configuration :

`before_script`

<img src="./img/before_script.png" style="width:600px" />

On installe Python et pip afin de pouvoir installer Docker-Compose dans le container créé par le runner avant de pouvoir procéder à nos différents jobs. La dernière étape est de lancer le docker compose.

`build`

<img src="./img/build.png" alt="build" style="width:600px" />

​	On lance le container de notre api en détaché et on exécute les commandes de lancement.



`test`

<img src="./img/ci-test.png" alt="ci-test" style="width:600px" />

Dans ce deuxième job, on lance notre api puis on éxecute les tests. Si cette phase passe, alors on pourra faire une merge request de cette branche.



`package`

<img src="./img/ci-package.png" alt="ci-package" style="width:600px" />

Ce job est éxécutée uniquement sur la branche master si le build et les tests se sont bien passés,Ce job consiste à construire une image de notre API de la publier sur le registry gitlab.



`deploy`

<img src="./img/deploy.png" alt="deploy" style="width:600px" />

Lors de la dernière phase qui n'intervient également que sur la branche master, on publie notre application sur un serveur de déploiement. Nous n'avons pas eu le temps d'effectuer cette phase durant la période de mise en situation, c'est pourquoi nous simulons un déploiement en l'affichant dans les logs.



**Le workflow est le suivant :**

<img src="./img/ci-cd.png" alt="scrum" style="width:800px" />




### f. Elaboration de la documentation:  <a id="doc"></a>


Si les développeurs n'aiment pas écrire de la documentation, cela fait tout de même partie de leur travail, et il est important de faciliter la mise en place de méthodes et d'outils afin de palier à cette problématique.

En effet lors de l'arrivée d'un nouveau membre dans l'équipe, il doit pouvoir trouver tous les éléments pour comprendre installer et travailler sur un projet.

Il existe un langage de markup appelé le **MarkDown**. Il est très utilisé pour toutes les documentations techniques et est interprété sur les hébergeurs de code pour s'afficher de manière harmonieuse.

La syntaxe du Markdown est très simple à apprendre et à intégrer. De nombreuses ressources sont présentes à ce sujet.

<div style="page-break-after: always;"></div>

**Écrire en Markdown :**

Les titres

```md
# Titre H1
## Titre H2
### Titre H3
```

Les listes

```md
* item1
* item 2
	* item 2.1
	* item 2.2
```

Un lien
```md
[Titre du lien](URL du lien)
```

Un image
```md
![Titre de l'image](URL de l'image)
```

Un mot en gras
```md
un mot en **gras**
```

Un mot en italique
```md
un mot en _italique_
```

Une citation
```md
> ma citation
```

Un bloc de code
```md
​```js
console.log('Kaixo !');
​```
```

Aussi la documentation technique (installation, usage) de chaque projet est écrite en MarkDown dans un fichier `README.md` présent à la racine du projet.



<img src="./img/readme.png" alt="README.md" style="width:800px" />



D'autre part, certaines parties de l'application comme la documentation d'une API requiert une documentation externe au projet car les équipes travaillant sur l'application web n'ont pas automatiquement les droits d'accès au repository de l'API. 

De nombreux outils existent pour générer la documentation d'une API et nous avons choisi de la générer avec **Postman** car cet outil est très simple à prendre en main et il est très régulièrement utilisé pendant le développement d'une API afin de tester le fonctionnement et le retour des requêtes.



**Installation et usage de Postman**

1. Téléchargez le fichier d'installation sur le [site officiel de Postman](https://www.postman.com/downloads/) et lancez l'installation.

2. Lancez Postman _(Notez que lors de la première utilisation vous devez vous enregistrer)_.

3. Créez une collection correspondant à votre API

4. Ajouter à votre collection les requêtes correspondant à toutes les méthodes (**CRUD**) et routes de votre API (**endpoints**). Ne pas oublier de renseigner les headers et, le cas échéant, les params ou encore le body.

  

  <img src="./img/postman.png" alt="postman" style="width:800px" />

5. Une fois vos requêtes sauvegardées dans votre collection, faites un clic droit sur celle ci et cliquez sur "Publish Doc"

6. Renseigner les champs de configuration (hébergeur, style, ...), valider et c'est publié !

   

C'est de cette manière que nous avons publiée la doc de notre API consultable à cette adresse [https://documenter.getpostman.com/view/2359244/T1DnhJ6b?version=latest](https://documenter.getpostman.com/view/2359244/T1DnhJ6b?version=latest)




<img src="./img/docu.png" alt="docu" style="width:800px" />





## 3. Coordination du projet et pistes d'amélioration <a id="pistes"></a>

### a. Assistance utilisateur <a id="helpdesk"></a>



Une fois l'application déployée, sa vie n'est pas terminée et à chaque nouveau sprint, on développe de nouvelles fonctionalitées ou correctifs qui sont demandés par le client.

Afin de s'y retrouver et de pouvoir planifier et prioriser l'ajout de ses nouvelles fonctionnalités, il est intéressant de s'appuyer sur un système de ticketting qui pourra être renseigné par le support, l'équipe technique ou encore par une tâche automatisée à la détection de bugs.

Le service de ticketting proposé par Gitlab est très performant et permet d'être lié à des logiciels de gestion de support comme GLPI ou encore GestSup, ce qui est très intéressant car le service support renseigne les demandes dans son logiciel, et les tickets sont automatiquement transférés dans la liste Backlog de notre board Gitlab avec leur criticité et leur priorisation.



<img src="./img/glpi.jpg" alt="glpi" style="width:500px" />
																													



Le service de support a également accès en lecture et en écriture à une base de connaissance qui documente les solutions apportées aux différents problèmes rencontrés, ce qui lui permet d'intervenir plus rapidement sur certaines pannes.

Cette base de connaissance peut être mise en place grâce au **Wiki** de Gitlab ou l'on peut y intégrer de la documentation en MarkDown.



De plus afin de maximiser la qualité des services de l'entreprise, il est conseillé de fournir au client une documentation complète incluant un **RACI** _(Responsible, Accountable, Consulted, Informed)_ qui permet d'identifier les rôles de chaque personnes dans l'évolution du projet. Le service support peut également s'y référer.

<img src="./img/raci.png" alt="raci" style="width:500px" />

Dans l'exemple ci dessus, pour la tâche n°2, La personne C est celle qui est **"Responsible"**, c'est à dire c'est elle qui est en charge de réaliser la tâche. Il peut y avoir plusieurs R sur une même tâche.

La personne B à le rôle d' **"Accountable"**, c'est à dire que c'est la responsable de la tâche effectuée, celle qui doit rendre des comptes. Il ne peut y avoir qu'un A par tâches.

La personne A est pour sa part **"Consulted"**. C'est elle qui est consultée au sujet de cette tâche. Il peut y en avoir plusieurs.

Et pour finir la personne D est **"Informed"**.  On informe cette personne que cette tâche à été éffectuée. Cette entité n'est pas obligatoire et peut être multiple.

### b. Aspects juridiques <a id="juridique"></a>



Depuis le 25 mai 2018 les **RGPD** _(Règlementation sur la Protection des Données)_ est appliquée. 

Le développement logiciel en est fortement impacté de par le fait que la quasi totalité des logiciels utilisent des bases de données. 



<img src="./img/rgpd.jpg" alt="rgpd" style="width:500px" />



Les principes fondamentaux des RGPD sont:

* Le droit à l'effacement _(Les utilisateurs ont le droit de demander l'effacement total de leurs données)_

* Le droit d'opposition _(Les utilisateurs peuvent s'opposer à l'utilisation de leurs données)_

* Le droit à la portabilité _(Les utilisateurs peuvent réclammer leurs données dans un format lisible)_

* Le droit à la modification _(Les utilisateurs peuvent recifier des données personnelles)_

  

Dans l'état actuel de notre base de donnée, nous n'intégrons pas encore de données personnelles mais il est important d'anticiper et d'intégrer les principes des RGPD dans les futurs développements et de les inscrire au sein d'une charte informatique. 




## Conclusion <a id="conclusion"></a>



A la suite de l'intégration de tous ces outils et méthodes de développement, la société Massive Data aura un processus de livraison continue stable, documenté, attractif et surtout automatisé ce qui fera gagner un temps considérable aux équipes techniques qui pourront alors se concentrer sur le vif de leur métier et apporter en continue de la valeur ajoutée aux projets sur lesquels ils travaillent.
